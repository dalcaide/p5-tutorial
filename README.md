

# P5 tutorial
### Date 07/03/17

## Setting up p5 environment

- *[Recommended]* P5 editor <https://github.com/processing/p5.js-editor/release>

- Other text editor <https://p5js.org/get-started/>
