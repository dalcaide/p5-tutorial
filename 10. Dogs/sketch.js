var dog1, dog2;

var dog = function(n,b,w) {
  this.name = n
  this.breed = b
  this.weight = w

  this.bark = function() {
    console.log("My name is " + this.name + ", I'm a " + this.breed + ", and I weigh " + this.weight + " kg")
  }

  this.eat = function() {
    console.log("I am " + this.name + " and I ate")
  }

  this.pee = function() {
    console.log("I am " + this.name + " and I've got wet legs now...")
  }
};

function setup() {
  createCanvas(710, 400);

  dog1 = new dog("Buddy", "Rottweiler", 19);
  dog2 = new dog("Lucy","Terrier",8)

  dog1.bark()
  dog1.eat()
  dog1.pee()
  dog2.bark()
  dog2.eat()
  dog2.pee()
}
