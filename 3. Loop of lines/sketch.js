function setup() {
  createCanvas(500,150)
  startX = 100 // <-- start position of the line
  stopX = 400 // <-- stop position of the line
  background(255,255,255);
  /* TODO:
     Create a for loop to draw ten horizontal lines from 0 to 100
     with steps of 10 pixels.
     Add a line using the variable startX as x1 and stopX as x2.
     Hint:
        for statement: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration
        line(x1, y1, x2, y2)
  */
}
