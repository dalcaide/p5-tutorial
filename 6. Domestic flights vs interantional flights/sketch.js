var table;

function preload() {
  table = loadTable("flights.csv","csv","header")
}
function setup() {
  createCanvas(600,300)
  noStroke()
  fill(0,0,255,10)

  background(255,255,255)
  var rows = table.getRows()
  for (var r = 0; r < rows.length; r++) {
    var from_long = rows[r].getNum("from_long")
    var from_lat = rows[r].getNum("from_lat")
    var from_country = rows[r].getString("from_country")
    var to_country = rows[r].getString("to_country")
    var distance = rows[r].getNum("distance")

    var x = map(from_long,-180,180,0,width)
    var y = map(from_lat,-90,90,height,0)
    /* TODO:
    Add a condition to distinguish international
    from domestic flights.
    You can revise the exercise number 4 to know how to
    write conditions in javascript.
    Use to color red for domestic flights and color
    blue for international flights.
    */
    var radius = map(distance,1,15406,3,15)
    ellipse(x,y,radius,radius)
  }
}
