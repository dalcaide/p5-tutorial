var my_flight;

var flight = function(d,flo,fla,tlo,tla,fc,tc) {
  this.distance = d
  this.from_long = flo
  this.from_lat = fla
  this.to_long = tlo
  this.to_lat = tla
  this.from_country = fc
  this.to_country = tc

  this.departureX = map(this.from_long, -180,180,0,width)
  this.departureY = map(this.from_lat, -90,90,height,0)

  this.drawDepartureAirport = function() {
    ellipse(this.departureX, this.departureY, 5,5)
  }
}

function setup() {
  createCanvas(800, 400);
  noStroke()
  fill(255,0,0,50)
  my_flight = new flight(1458, 61.838, 55.509, 38.51, 55.681, "Belgium", "Germany")
}

function draw() {
  background(255,255,255)
  my_flight.drawDepartureAirport()
}
