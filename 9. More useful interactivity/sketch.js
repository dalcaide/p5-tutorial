var table;

function preload() {
  table = loadTable("flights.csv","csv","header")
}
function setup() {
  createCanvas(600,300)
  noStroke()
  noLoop()
}

function draw() {
  background(255,255,255)
  var rows = table.getRows()
  for (var r = 0; r < rows.length; r++) {
    /*
    TODO
    we want to use the mouse pointer as a filter.
    For example: if our mouse is at the left only short
    distance flights are drawn; if our mouse is at the
    right only long distance flights are drawn.

    We will only draw flights if their duration is between
    a calculated minDistance and maxDistance
    */

    var distance = rows[r].getNum("distance")
    var mouseXMin = mouseX - 25
    var mouseXMax = mouseX + 25
    var minDistance = map(mouseXMin, -25, 625, 0, 15406)
    var maxDistance = map(mouseXMax, -25, 625, 0, 15406)

    var from_long = rows[r].getNum("from_long")
    var from_lat = rows[r].getNum("from_lat")
    var from_country = rows[r].getString("from_country")
    var to_country = rows[r].getString("to_country")
    var distance = rows[r].getNum("distance")

    var x = map(from_long,-180,180,0,width)
    var y = map(from_lat,-90,90,height,0)
    if ( from_country == to_country ) {
      fill(255,0,0,10)
    } else {
      fill(0,0,255,10)
    }
    ellipse(x,y,5,5)
  }
}
function mouseMoved() {
  redraw()
  return false
}
