function setup() {
  createCanvas(500,150)
  startX = 100 // <-- start position of the line
  stopX = 400 // <-- stop position of the line
  background(255,255,255);
  strokeWeight(2);
  for (var i = 0; i < 100; i+=10 ) {
    if ( i%20 == 0 ) {
      stroke(255,0,0);
    } else {
      stroke(0,0,255);
    }
    line(startX, i, stopX, i)
  }
}
