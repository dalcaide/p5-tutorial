var table;

function preload() {
  table = loadTable("flights.csv","csv","header")
}
function setup() {
  createCanvas(600,300)
  noStroke()
  noLoop()
}

function draw() {
  background(255,255,255)
  var rows = table.getRows()
  for (var r = 0; r < rows.length; r++) {
    var from_long = rows[r].getNum("from_long")
    var from_lat = rows[r].getNum("from_lat")
    var from_country = rows[r].getString("from_country")
    var to_country = rows[r].getString("to_country")
    var distance = rows[r].getNum("distance")

    var x = map(from_long,-180,180,0,width)
    var y = map(from_lat,-90,90,height,0)
    if ( from_country == to_country ) {
      fill(255,0,0,10)
    } else {
      fill(0,0,255,10)
    }
    /* TODO
    We want the radius of the dots to depend on the position
    of the mouse instead of the distance of the flight.
    p5 provides two variables called mouseX and
    mouseY that are very useful.
    mouseX returns the x position of the pointer.
    create the variable radius that maps the position of the mouse
    and returns a value between 3 and 15.
    Hint: documentation for map function: 
    https://p5js.org/reference/#/p5/map
    */
    ellipse(x,y,radius,radius)
  }
}

function mouseMoved() {
  redraw()
  return false
}
