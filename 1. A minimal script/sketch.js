// The statements in the setup() function
// execute once when the program begins
function setup() {

  // createCanvas sets the dimensions of the resulting image.
  createCanvas(400, 400) // (width, height)

  // fill set the colour of anything we draw
  // fill (red, green, blue) <-- ranging from 0 to 255
  fill(255,0,0)


  // Note that the vertical position counts from the top down instead of from the bottom up.
  // The point (0,0) is at the top left rather than the bottom left
  ellipse(100,150,20,20) // ellipse(x1, y1, width, height)

  fill(0,255,0)
  rect(200,200,50,60) // rect(x1, y1, width, height)

  stroke(0,0,255)
  strokeWeight(5)
  line(150,5,150,50) // line(x1, y1, x2, y2)
}

 // Note: Both fill and stroke are used to set colour: fill to set the colour of the shape,
 // stroke to set the colour of the border around that shape. In case of lines, only the stroke
 // color can be set.

// Reference:  https://p5js.org/reference/
